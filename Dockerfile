FROM ruby:2.3

ADD . /var/services/pypi-graphql
WORKDIR /var/services/pypi-graphql

RUN ./scripts/setup
CMD ./scripts/init
