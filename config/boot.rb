require 'bundler/setup'
require 'sinatra/cross_origin'
require 'graphql/schema/pypi'
require 'json'

require './app'
